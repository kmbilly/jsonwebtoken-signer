var util = require('util');
var base64url = require('./base64url');
var toString = require('./tostring');

function jwsSecuredInput(header, payload, encoding) {
  encoding = encoding || 'utf8';
  var encodedHeader = base64url(toString(header), 'binary');
  var encodedPayload = base64url(toString(payload), encoding);
  return util.format('%s.%s', encodedHeader, encodedPayload);
}

function jwsSign(opts) {
  var header = opts.header;
  var payload = opts.payload;
  var signer = opts.signer;
  var encoding = opts.encoding;
  var securedInput = jwsSecuredInput(header, payload, encoding);
  var signature = signer(securedInput);
  if (typeof signature === 'string') {
    return util.format('%s.%s', securedInput, signature);
  } else {
    return signature.then(
      signResult => util.format('%s.%s', securedInput, signResult)
    );
  }
}

module.exports = jwsSign;
