# jsonwebtoken-signer

A clone of jsonwebtoken library but supports external signer like AWS KMS

# Install

```bash
$ npm install jsonwebtoken-signer
```

# Usage

Same as the usage of jsonwebtoken, except that jwt.sign can specify a signing function instead  

var jwt = require('jsonwebtoken-signer')

### jwt.sign(payload, secretOrPrivateKey, [options, callback])

`secretOrPrivateKey` could be a signing function that accepts a raw string to sign and return base64url formatted signature, so that you can call external signer such as AWS KMS to sign the string

The signing function could return a promise also, then jwt.sign would return a promise as well

A base64url function is also available as jwt.base64url

## License

This project is licensed under the MIT license. See the [LICENSE](LICENSE) file for more info.
